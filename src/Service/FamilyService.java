package Service;

import DAO.CollectionFamilyDao;
import DAO.FamilyDao;
import Entity.*;

import java.time.Instant;
import java.time.Year;
import java.time.ZoneId;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

class FamilyOverflowException extends RuntimeException{

}

public class FamilyService { // CONTAINS ALL LOGIC, IF IS POSSIBLE THEN PROCEEDS TO DAO AND FURTHER, IF NOT RETURNS BACK TO CONTROLLER

    private final FamilyDao familyDao = new CollectionFamilyDao();

    public int count() {
        return familyDao.count();
    }

    public List<Family> getAllFamilies() {

        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(family -> System.out.println(family.prettyFamily()));
    }

    private int sizeChecker() {
        System.out.println("Enter family index: ");
        int index;
        while (true) {
            index = intCheckerForFamilyMembers();
            if (0 <= index && index < familyDao.getAllFamilies().size()) {
                break;
            } else {
                System.out.println("Enter index from an existing interval");
                System.out.println("Last available index is: " + (familyDao.getAllFamilies().size() - 1));
            }
        }
        return index;
    }

    private int intCheckerForFamilyMembers() {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Enter exact number");
        while (true) {
            try {
                number = scanner.nextInt();
                break;
            } catch (InputMismatchException inputMismatchException) {
                System.out.println("Please enter an integer, not negative number");
                scanner.next();
            }
        }
        return number;
    }

    public void getFamiliesBiggerThan() {

        int finalNumber = intCheckerForFamilyMembers();
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > finalNumber).
                forEach(family -> System.out.println(family.prettyFamily()));
    }

    public void getFamiliesLessThan() {

        int finalNumber = intCheckerForFamilyMembers();
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < finalNumber).
                forEach(family -> System.out.println(family.prettyFamily()));
    }

    public void countFamiliesWithMemberNumber() {

        int finalNumber = intCheckerForFamilyMembers();
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == finalNumber).
                forEach(family -> System.out.println(family.prettyFamily()));
    }

    public String getFamilyByIndex(int index) {
        try {
            System.out.println(familyDao.getFamilyByIndex(index));
            return familyDao.getFamilyByIndex(index).prettyFamily();
        } catch (Exception e) {
            System.out.println("null");
            return null;
        }
    }

    public boolean deleteFamilyByIndex() {
        boolean deleted;
        try {
            int index = intCheckerForFamilyMembers();
            familyDao.deleteFamily(index);
            System.out.println("true");
            deleted = true;
        } catch (Exception e) {
            System.out.println("false");
            deleted = false;
        }
        return deleted;
    }

    public boolean deleteFamilyByFamily(Family family) {
        boolean deleted;
        try {
            if (familyDao.getAllFamilies().contains(family)) {
                familyDao.deleteFamily(family);
                System.out.println("true");
                deleted = true;
            } else {
                System.out.println("false");
                deleted = false;
            }
        } catch (Exception e) {
            System.out.println("false");
            deleted = false;
        }
        return deleted;
    }

    public void saveFamily(Family family) {
        if (familyDao.getAllFamilies().contains(family)) {
            int permanentIndex = familyDao.getFamilyIndex(family);
            familyDao.deleteFamily(familyDao.getFamilyIndex(family));
            familyDao.saveFamilyByIndex(permanentIndex, family);
        } else
            familyDao.saveFamily(family);
    }

    public void createNewFamily() {
        Human mother = new Human();
        Human father = new Human();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter mother's name: ");
        mother.setName(scanner.next());
        System.out.println("Enter mother's surname: ");
        mother.setSurname(scanner.next());
        System.out.println("Enter mother's birth year: ");
        String motherBirthYear = String.valueOf(intCheckerForFamilyMembers());
        System.out.println("Enter mother's birth month: ");
        String motherBirthMonth = String.valueOf(intCheckerForFamilyMembers());
        System.out.println("Enter mother's birth day: ");
        String motherBirthDay = String.valueOf(intCheckerForFamilyMembers());
        mother.setBirthdate(mother.StringDateToLong(motherBirthDay + "/" + motherBirthMonth + "/" + motherBirthYear));
        System.out.println("Enter mother's IQ: ");
        mother.setIq(intCheckerForFamilyMembers());
        System.out.println("Enter father's name: ");
        father.setName(scanner.next());
        System.out.println("Enter father's surname: ");
        father.setSurname(scanner.next());
        System.out.println("Enter father's birth year: ");
        String fatherBirthYear = String.valueOf(intCheckerForFamilyMembers());
        System.out.println("Enter father's birth month: ");
        String fatherBirthMonth = String.valueOf(intCheckerForFamilyMembers());
        System.out.println("Enter father's birth day: ");
        String fatherBirthDay = String.valueOf(intCheckerForFamilyMembers());
        father.setBirthdate(father.StringDateToLong(fatherBirthDay + "/" + fatherBirthMonth + "/" + fatherBirthYear));
        System.out.println("Enter father's IQ: ");
        father.setIq(intCheckerForFamilyMembers());
        System.out.println("Enter family name: ");
        Family family = new Family(scanner.next());
        family.setMother(mother);
        family.setFather(father);
        familyDao.saveFamily(family);
    }


    public Family bornChild() {
        int index = sizeChecker();
        System.out.println("For newborn baby boy press 1: ");
        System.out.println("For newborn baby girl press 2: ");
        while (true) {
            int detector = intCheckerForFamilyMembers();
            if (detector == 1) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Enter child's name: ");
                String boyName = scanner.next();
                System.out.println();
                System.out.println("Enter birth year: ");
                String birthYear = String.valueOf(intCheckerForFamilyMembers());
                System.out.println("Enter birth month: ");
                String birthMonth = String.valueOf(intCheckerForFamilyMembers());
                System.out.println("Enter birth day: ");
                String birthDay = String.valueOf(intCheckerForFamilyMembers());
                String birthdate = birthDay + "/" + birthMonth + "/" + birthYear;
                System.out.println("Enter child's IQ: ");
                int iq = intCheckerForFamilyMembers();
                familyDao.getFamilyByIndex(index).addChild(new Man(boyName, familyDao.getFamilyByIndex(index).getFather().getSurname(), birthdate, iq, familyDao.getFamilyByIndex(index).getPet()));
                break;
            } else if (detector == 2) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Enter child's name: ");
                String girlName = scanner.next();
                System.out.println("Enter birth year: ");
                String birthYear = String.valueOf(intCheckerForFamilyMembers());
                System.out.println("Enter birth month: ");
                String birthMonth = String.valueOf(intCheckerForFamilyMembers());
                System.out.println("Enter birth day: ");
                String birthDay = String.valueOf(intCheckerForFamilyMembers());
                String birthdate = birthDay + "/" + birthMonth + "/" + birthYear;
                System.out.println("Enter child's IQ: ");
                int iq = intCheckerForFamilyMembers();
                familyDao.getFamilyByIndex(index).addChild(new Woman(girlName, familyDao.getFamilyByIndex(index).getFather().getSurname(), birthdate, iq, familyDao.getFamilyByIndex(index).getPet()));
                break;
            } else {
                System.out.println("Enter one of two highlighted numbers.");
            }
        }
        return familyDao.getFamilyByIndex(index);
    }

    public String adoptChild(Family family, Human adoptedChild) {   // TODO : refactor
        int index = familyDao.getFamilyIndex(family);
        familyDao.getFamilyByIndex(index).addChild(adoptedChild);
        return familyDao.getFamilyByIndex(index).prettyFamily();
    }

    public void deleteAllChildrenOlderThan() {
        int number = intCheckerForFamilyMembers();
        for (Family family : getAllFamilies()) {
            List<Human> humanList = family.getChildrenList().stream().filter(human -> Year.now().getValue() -
                    Instant.ofEpochMilli(human.getBirthdate()).atZone(ZoneId.systemDefault()).toLocalDate().getYear() > number).collect(Collectors.toList());
            humanList.forEach(family::deleteChild);
        }
    }

    public String getFamilyById(int id) {
        try {
            System.out.println(familyDao.getFamilyByIndex(id));
            return familyDao.getFamilyByIndex(id).prettyFamily();
        } catch (Exception e) {
            System.out.println("No matches found.");
            return null;
        }
    }

    public Set<Pet> getPets(int index) {
        try {
            return familyDao.getFamilyByIndex(index).getPetList();
        } catch (Exception e) {
            System.out.println("No matches found.");
            return null;
        }
    }

    public void addPet(int index, Pet pet) {
        try {
            familyDao.getFamilyByIndex(index).setPet(pet);
        } catch (Exception e) {
            System.out.println("No matches found.");
        }
    }

    public void operationsList() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Select operation number for needed actions. See descriptions below.");
        System.out.println("1. Fill with test data (create several families and save them in the database)");
        System.out.println("2. Display the entire list of families (displays a list of all families with indexation starting with 1)");
        System.out.println("3. Display a list of families where the number of people is greater than the specified number\n" +
                "  - request a number one you interested in\n");
        System.out.println("4. Display a list of families where the number of people is less than the specified number\n" +
                "  - request a number one you interested in\n");
        System.out.println("5. Calculate the number of families where the number of members is\n" +
                "  - request a number one you interested in\n");
        System.out.println("6. Create a new family");
        System.out.println("7. Delete a family by its index in the general list\n" +
                "  - request identifier (ID)\n");
        System.out.println("8. Edit a family by its index in the general list\n" +
                "  - 1. Give birth to a baby\n" +
                "    - request family identifier (ID)\n" +
                "    - request the necessary data (what name to give the boy, what name to girl)\n" +
                "  - 2. Adopt a child\n" +
                "    - request family identifier (ID)\n" +
                "    - request Required data (full name, year of birth, intelligence)\n" +
                "  - 3. Return to main menu  \n");
        System.out.println("9. Remove all children over the age of majority\n" +
                "  - request interested age\n");
        boolean decision = true;
        while (decision) {
            int operationsNumber = intCheckerForFamilyMembers();
            switch (operationsNumber) {

                case 1:
                    System.out.println("You are in category 1: ");
//
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 2:
                    System.out.println("You are in category 2: ");
                    displayAllFamilies();
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 3:
                    System.out.println("You are in category 3: ");
                    getFamiliesBiggerThan();
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 4:
                    System.out.println("You are in category 4: ");
                    getFamiliesLessThan();
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 5:
                    System.out.println("You are in category 5: ");
                    countFamiliesWithMemberNumber();
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 6:
                    System.out.println("You are in category 6: ");
                    createNewFamily();
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 7:
                    System.out.println("You are in category 7: ");
                    deleteFamilyByIndex();
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 8:
                    System.out.println("You are in category 8: ");
                    while (true) {
                        int subcategory = intCheckerForFamilyMembers();
                        switch (subcategory) {
                            case 1:
                                bornChild();
                                break;
                            case 2:
//                                adoptChild();
                                break;
                            case 3:
                                break;
                        }
                        break;
                    }
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 9:
                    System.out.println("You are in category 9: ");
                    deleteAllChildrenOlderThan();
                    System.out.println("You are redirected to the main branch.");
                    break;
                case 0:
                    decision =false;
            }
            System.out.println("You have closed dialogue window.");
        }
    }

}

