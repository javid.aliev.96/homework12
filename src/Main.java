import Controllers.FamilyController;
import Entity.*;

public class Main {

    private final static FamilyController familyController = new FamilyController();

    public static void main(String[] args) {

        Dog rex = new Dog("Rex", 4, 99, new String[]{"eat, sleep, run"});
        DomesticCat garfield = new DomesticCat("Garfield", 3, 99, new String[]{"trick Rex, take a nap, play with ball"});

        Human test1 = new Human("Anna", "Korotchenko", "18/02/1970", 99);
        Human test2 = new Human("Vladimir", "Korotchenko", "14/10/1959", 99);
        Human test3 = new Human("Yelena", "Korotchenko", "28/01/1996", 99);
        Human test4 = new Human("Dmitriy", "Korotchenko", "04/04/2004", 66);
        Human test5 = new Human("Katerina", "Korotchenko", "05/05/2005", 66);
        Human test6 = new Human("Nikolay", "Korotchenko", "07/07/2007", 66);
        Human test7 = new Human("Aleksey", "Korotchenko", "01/01/2001", 66);

        Family testFamily = new Family("TestFamily");
        testFamily.setMother(test1);
        testFamily.setFather(test2);
        testFamily.setPet(rex);
        testFamily.setPet(garfield);

        familyController.saveFamily(testFamily);
        familyController.adoptChild(testFamily, test3);
        familyController.adoptChild(testFamily, test7);
        familyController.adoptChild(testFamily, test6);
        familyController.adoptChild(testFamily, test4);
        familyController.adoptChild(testFamily, test5);
        test3.setGender(Gender.WOMAN);
        test4.setGender(Gender.MAN);
        test5.setGender(Gender.WOMAN);
        test6.setGender(Gender.MAN);
        test7.setGender(Gender.MAN);

        familyController.operationsList();

    }
}






