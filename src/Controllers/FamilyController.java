package Controllers;

import Entity.Family;
import Entity.Human;
import Entity.Pet;
import Service.FamilyService;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class FamilyController {

    private final FamilyService familyService = new FamilyService();

    public int count() {
        return this.familyService.count();
    }

    public String getFamilyByIndex(int index) {
        return this.familyService.getFamilyByIndex(index);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() { familyService.displayAllFamilies(); }

    public void getFamiliesBiggerThan() {
        familyService.getFamiliesBiggerThan();
    }

    public void getFamiliesLessThan() {
        familyService.getFamiliesLessThan();
    }

    public void countFamiliesWithMemberNumber() {
        familyService.countFamiliesWithMemberNumber();
    }

    public boolean deleteFamilyByIndex() {
        return familyService.deleteFamilyByIndex();
    }

    public boolean deleteFamilyByFamily(Family family) {
        return familyService.deleteFamilyByFamily(family);
    }

    public void createNewFamily() {
        familyService.createNewFamily();
    }

    public Family bornChild() { return familyService.bornChild(); }

    public String adoptChild(Family family, Human adoptedChild) {
        return familyService.adoptChild(family, adoptedChild);
    }

    public void deleteAllChildrenOlderThan() {
        familyService.deleteAllChildrenOlderThan();
    }

    public String getFamilyById(int id) {
        return familyService.getFamilyById(id);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public void operationsList(){ familyService.operationsList();}
}
