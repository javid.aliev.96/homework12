package Entity;

public class Dog extends Pet implements Foul {
    public Dog(String nickname, int ageOfPet, int trickLevel, String[] habits) {
        super(nickname, ageOfPet, trickLevel, habits);
        super.setSpecies(Species.Dog);
    }


    @Override
    public void respond() {
        System.out.println("Hello owner. I am a Entity.Dog and my name is " + getNickname() + ". I miss you!");
    }

    @Override
    public void foul() {
        System.out.println("Entity.Dog created a mess, Doggo needs to cover it up");
    }
}
