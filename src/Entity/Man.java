package Entity;

public final class Man extends Human {
    private Pet pet;

    public Man(String name, String surname, String birthdate, int iq, Pet pet) {
        super(name, surname, birthdate, iq);
        this.pet = pet;
        super.setGender(Gender.MAN);
    }

    public Man(String name, String surname, String birthdate, int iq) {
        super(name, surname, birthdate, iq);
    }

    public Man() {
    }

    @Override
    public void scheduleSetter(String dayOfWeek, String tasks) {
        super.scheduleSetter(dayOfWeek, tasks);
    }

    @Override
    public void scheduleGetter() {
        super.scheduleGetter();
    }

    public void fedUp() {
        System.out.println("What is wrong again?!");
    }

    @Override
    public void greetPet() {
        System.out.println("Hey buddy, " + this.pet.getNickname() + ", come here.");
    }

}